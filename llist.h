/*
 * property of meylingtaing
 * source: https://gist.github.com/meylingtaing/11018042
 */

/* llist.h
 * Generic Linked List
 */

#ifndef COMP30023_2021_PROJECT_1_LLIST_H
#define COMP30023_2021_PROJECT_1_LLIST_H


struct node {
    void *data;
    struct node *next;
};

typedef void *llist;

/* llist_create: Create a linked list */
llist *llist_create(void *data);

/* llist_free: Free a linked list */
void llist_free(llist *list);

/* llist_add_inorder: Add to sorted linked list */
int llist_add_inorder(void *data, llist *list,
                      int (*comp)(void *, void *));

/* llist_push: Add to head of list */
void llist_push(llist *list, void *data);

/* llist_pop: remove and return head of linked list */
void *llist_pop(llist *list);

/* llist_print: print linked list */
void llist_print(llist *list, void (*print)(void *data));

#endif //COMP30023_2021_PROJECT_1_LLIST_H
